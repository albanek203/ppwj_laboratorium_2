package Exercise_2;

public class Ex_f {
    public static void main(String[] args) {
        var array = TestTable.Generate(24, -999, 999);
        SigNumbers(array);
    }

    public static void SigNumbers(int[] array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] >= 0)
                array[i] = 1;
            else array[i] = -1;
            System.out.print(array[i] + " ");
        }
    }
}
