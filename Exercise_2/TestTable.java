package Exercise_2;

import java.util.Random;

public class TestTable {
    public static void main(String[] args) {
    }

    public static int[] Generate(int n, int minValue, int MaxValue) {
        var table = new int[n];
        var random = new Random();
        for (var i = 0; i < n; i++)
            table[i] = random.nextInt((MaxValue - (minValue)) + 1) + minValue;
        return table;
    }
}
