package Exercise_1;

import java.util.Random;
import java.util.Scanner;

public class Ex_e {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        var n = input.nextInt();

        if (n < 1 || n > 100) {
            System.out.println("N must be from 1 to 100");
            return;
        }

        var array = new int[n];
        var random = new Random();
        for (var i = 0; i < n; i++)
            array[i] = random.nextInt((999 - (-999)) + 1) - 999;

        var length = 0;
        for (var item : array) {
            if (item >= 0) {
                var tmpLength = Integer.toString(item).length();
                if (tmpLength > length)
                    length = tmpLength;
            }
        }

        System.out.println("Length: " + length);
    }
}
