package Exercise_2;

public class Ex_a {
    public static void main(String[] args) {
        var array = TestTable.Generate(24, -999, 999);
        var countEven = countEven(array);
        var countOdd = countOdd(array);
        System.out.println("Count even: " + countEven);
        System.out.println("Count odd: " + countOdd);
    }

    public static int countOdd(int[] array) {
        var result = 0;
        for (var item : array)
            if (item % 2 != 0)
                result++;
        return result;
    }
    public static int countEven(int[] array) {
        var result = 0;
        for (var item : array)
            if (item % 2 == 0)
                result++;
        return result;
    }
}
