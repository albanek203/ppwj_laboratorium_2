package Exercise_3;

import Exercise_2.TestTable;

import java.util.Random;
import java.util.Scanner;

public class Exercise {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m, n, k = 0;
        while (true) {
            System.out.print("Enter m:");
            m = input.nextInt();
            if (m >= 1 && m <= 10)
                break;
            System.out.print("m must be from 1 to 10:");
        }
        while (true) {
            System.out.print("Enter n:");
            n = input.nextInt();
            if (n >= 1 && n <= 10)
                break;
            System.out.print("n must be from 1 to 10:");
        }
        while (true) {
            System.out.print("Enter k:");
            k = input.nextInt();
            if (k >= 1 && k <= 10)
                break;
            System.out.print("k must be from 1 to 10:");
        }
        var random = new Random();

        var matrixA = new int[m][n];
        for (var i = 0; i < m; i++)
            for (var j = 0; j < n; j++)
                matrixA[i][j] = random.nextInt((99 - (10)) + 1) + 10;

        var matrixB = new int[n][k];
        for (var i = 0; i < n; i++)
            for (var j = 0; j < k; j++)
                matrixB[i][j] = random.nextInt((99 - (10)) + 1) + 10;

        System.out.println("Matrix A:");
        ShowMatrix(matrixA, m, n);
        System.out.println("Matrix B:");
        ShowMatrix(matrixB, n, k);

        if (m != n && n != k) {
            System.out.println("Matrices cannot be multiplied");
            return;
        }

        var matrixС = new int[m][n];
        for (int i = 0; i < n; i++)
            for (int u = 0; u < k; u++)
                for (int j = 0; j < m; j++)
                    matrixС[i][u] += matrixA[i][j] * matrixB[j][u];

        System.out.println("Matrix C:");
        ShowMatrix(matrixС, m, n);
    }

    private static void ShowMatrix(int[][] matrix, int m, int n) {
        for (var i = 0; i < m; i++) {
            for (var j = 0; j < n; j++)
                System.out.print(matrix[i][j] + " ");
            System.out.println("");
        }
    }
}
