package Exercise_2;

public class Ex_e {
    public static void main(String[] args) {
        var array = TestTable.Generate(24, -999, 999);
        var length = LengthMaxValuePositive(array);
        System.out.println("Length: " + length);
    }

    public static int LengthMaxValuePositive(int[] array) {
        var length = 0;
        for (var item : array) {
            if (item >= 0) {
                var tmpLength = Integer.toString(item).length();
                if (tmpLength > length)
                    length = tmpLength;
            }
        }
        return length;
    }
}
