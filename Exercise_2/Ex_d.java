package Exercise_2;

public class Ex_d {
    public static void main(String[] args) {
        var array = TestTable.Generate(24, -999, 999);
        var sumPositive = SumPositive(array);
        var sumNegative = SumNegative(array);
        System.out.println("Sum positive: " + sumPositive);
        System.out.println("Sum negative: " + sumNegative);
    }

    public static int SumPositive(int[] array) {
        var result = 0;
        for (var item : array)
            if (item >= 0)
                result += item;
        return result;
    }

    public static int SumNegative(int[] array) {
        var result = 0;
        for (var item : array)
            if (item < 0)
                result += item;
        return result;
    }
}
