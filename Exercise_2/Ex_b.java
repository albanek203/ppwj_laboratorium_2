package Exercise_2;

public class Ex_b {
    public static void main(String[] args) {
        var array = TestTable.Generate(24, -999, 999);
        var countPositive = countPositive(array);
        var countNegative = countNegative(array);
        var countZero = countZero(array);
        System.out.println("Count positive: " + countPositive);
        System.out.println("Count negative: " + countNegative);
        System.out.println("Count zero: " + countZero);
    }

    public static int countPositive(int[] array) {
        var result = 0;
        for (var item : array)
            if (item % 2 == 0)
                result++;
        return result;
    }

    public static int countNegative(int[] array) {
        var result = 0;
        for (var item : array)
            if (item % 2 != 0)
                result++;
        return result;
    }

    public static int countZero(int[] array) {
        var result = 0;
        for (var item : array)
            if (item == 0)
                result++;
        return result;
    }
}
