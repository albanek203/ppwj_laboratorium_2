package Exercise_1;

import java.util.Random;
import java.util.Scanner;

public class Ex_b {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        var n = input.nextInt();

        if (n < 1 || n > 100) {
            System.out.println("N must be from 1 to 100");
            return;
        }

        var array = new int[n];
        var random = new Random();
        for (var i = 0; i < n; i++)
            array[i] = random.nextInt((999 - (-999)) + 1) - 999;

        var countPositive = 0;
        var countNegative = 0;
        var countZero = 0;
        for (var item : array) {
            if (item == 0)
                countZero++;
            else if (item > 0)
                countPositive++;
            else countNegative++;
        }

        System.out.println("Count positive: " + countPositive);
        System.out.println("Count negative: " + countNegative);
        System.out.println("Count zero: " + countZero);
    }
}
