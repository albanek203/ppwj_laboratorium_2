package Exercise_1;

import java.util.Random;
import java.util.Scanner;

public class Ex_g {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter count digit:");
        var n = input.nextInt();

        if (n < 1 || n > 100) {
            System.out.println("N must be from 1 to 100");
            return;
        }

        var array = new int[n];
        var random = new Random();
        for (var i = 0; i < n; i++)
            array[i] = random.nextInt((999 - (-999)) + 1) - 999;

        var right = 0;
        var left = 0;
        while (true) {
            System.out.print("Enter left:");
            left = input.nextInt();

            if (left >= 1 && left <= n)
                break;
            System.out.println("Left must be from 1 to " + n);
        }
        while (true) {
            System.out.print("Enter right:");
            right = input.nextInt();

            if (right >= 1 && right <= n && right > left)
                break;

            System.out.println("Right must be from 1 to " + n + "\nOr right must be bigger then left");
        }

        for (var i = left; i < right; i++)
            System.out.print(array[i] + " ");
    }
}
