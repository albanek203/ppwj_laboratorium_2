package Exercise_2;

import java.util.Scanner;

public class Ex_g {
    public static void main(String[] args) {
        var n = 24;
        var array = TestTable.Generate(n, -999, 999);
        Scanner input = new Scanner(System.in);
        var right = 0;
        var left = 0;
        while (true) {
            System.out.print("Enter left:");
            left = input.nextInt();

            if (left >= 1 && left <= n)
                break;
            System.out.println("Left must be from 1 to " + n);
        }
        while (true) {
            System.out.print("Enter right:");
            right = input.nextInt();

            if (right >= 1 && right <= n && right > left)
                break;

            System.out.println("Right must be from 1 to " + n + "\nOr right must be bigger then left");
        }
        ShowFromLeftToRight(array, left, right);
    }

    public static void ShowFromLeftToRight(int[] array, int left, int right) {
        for (var i = left; i < right; i++)
            System.out.print(array[i] + " ");
    }
}
