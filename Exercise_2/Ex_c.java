package Exercise_2;

public class Ex_c {
    public static void main(String[] args) {
        var array = TestTable.Generate(24, -999, 999);
        var countMaxValue = countMaxValue(array);
        System.out.println("Count max value: " + countMaxValue);
    }

    public static int countMaxValue(int[] array) {
        var maxValue = array[0];
        for (var item : array)
            if (item > maxValue)
                maxValue = item;

        var countMaxValue = 0;
        for (var item : array)
            if (item == maxValue)
                countMaxValue++;
        return countMaxValue;
    }
}
